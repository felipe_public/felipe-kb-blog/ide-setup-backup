# IDE Setup and Backup Tool
This application is installed as add on for your LabVIEW IDE, and helps you set up a custom config file for sharing, backup and easily reset your environment with your plugins.
Currently, this tool only works with LabVIEW Data Directory, usually located in User Documents Folder.

## Installation
- Use VIPM to install this utility.

## Usage
- In the created menu (FTools) you have the following options:
1. Setup Custom Configuration - Creates a JSON file based on your selected options. You have also the option to apply this configuration.
2. Apply Custom Configuration - Apply your configuration to LabVIEW IDE. It will attempt to restart LabVIEW.
3. Reset Custom Configuration - Read your configuration file and reset to the IDE default. It will attempt to restart LabVIEW.
4. Backup Files - This VI lets you choose what files and plugins you want to save in zip file.
5. Restore Files from Backup - This VI lets you choose what files and plugins you want to restore to your machine from a backup zip file.

## Compatibility and Dependencies
- LabVIEW 2015 or later.
- There are no dependencies.

## Author
- Felipe Pinheiro Silva