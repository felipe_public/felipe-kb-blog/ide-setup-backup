# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.2.1] - 2020-07-02
### Fix
- Labels and FP problems in Setup Custom Config.

## [0.2.0] - 2020-06-15
## Added
- New backup item: VI Analyzer User Dictionary.
- New VI for returning the file name from Label Text in Backup and Restore.
- Toolbar State Property to list non-visible toolbars.

### Changed
- Readme.md with some spelling corrections.

### Removed
- PalleteStyle needs changing in other parameters, therefore removed.

### Fix
- Spelling in right-click from Shortcut Menu Plugins.

## [0.1.0] - 2020-06-10
### Added
- Initial version.
